import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import moment from 'moment';
import Header from '../header/Header';
import MessageList from '../messageList/MessageList';
import MessageInput from '../messageInput/MessageInput'
import Preloader from '../preloader/Preloader';

export default function Chat({ url }) {
    const [dataChat, setDataChat] = React.useState([]);
    const [isLoading, setIsLoading] = React.useState(true)

    React.useEffect(() => {
        fetch(url).then(res => res.json()).then(res => {

            setDataChat(res);
            setIsLoading(false)

        });
    }, [url])

    // console.log(dataChat);

    let countAllMessages = dataChat.length

    let countAllUsers = new Set(dataChat.map((el) => {
        return el.userId
    })).size

    let timeLastMessage = moment(dataChat[dataChat.length - 1]?.createdAt).format('hh:mm')

    const addNewMessage = (newMsg) => {
        dataChat.push(newMsg);
        setDataChat([...dataChat]);
    }

    const deleteMessage = (id) => {
        console.log(id)
        let newDataChat = dataChat.filter((el) => el.id !== id);
        console.log(newDataChat)
        const confirmDelete = window.confirm("Вы точно хотите удалить?");

        confirmDelete && (setDataChat(newDataChat))
    }



    return (
        <div className="chat">
            <div className="container-fluid bg-dark bg-gradient ">
                <div className="row">
                    <div className="col-12">
                        <Header
                            allMessages={countAllMessages}
                            allUsers={countAllUsers}
                            timeLastMessage={timeLastMessage}
                        />
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-8">
                        {isLoading ? <Preloader /> : <MessageList messageArr={dataChat} deleteF={deleteMessage} />}
                        <MessageInput newMessage={addNewMessage} />
                    </div>
                </div>
            </div>
        </div>

    )
}
