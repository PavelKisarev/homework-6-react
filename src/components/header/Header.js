import React from 'react';
import './index.css';

export default function Header(props) {

    let lastTime
    if (props.timeLastMessage) {
        lastTime = props.timeLastMessage;
    }

    return (
        <>
            <header>
                <div className="header-title">
                    <div className="logo">My React CHAT</div>
                </div>

                <div className="header-chat-info">
                    <div className="header-users-count">
                        <span className="badge bg-info">{props.allUsers}</span>
                        participants
                    </div>
                    <div className="header-messages-count">
                        <span className="badge bg-info">{props.allMessages}</span>
                        messages
                    </div>
                </div>

                <div className="header-last-message-date">
                    last message at <span className="badge bg-info">{lastTime}</span>
                </div>
            </header>
        </>
    )
}
