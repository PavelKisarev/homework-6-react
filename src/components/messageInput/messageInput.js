import React from 'react';
import './index.css';
import { v4 as uuidv4 } from 'uuid';
import moment from 'moment';

export default function MessageInput(props) {

    let [message, setMessage] = React.useState('');

    const messageHandler = (e) => {
        setMessage(e.target.value)
    }

    const clickHandler = () => {
        props.newMessage({ id: uuidv4(), text: message, createdAt: moment().format('hh:mm'), own: true });
        setMessage('')
    }

    return (
        <div className="message-input">
            <textarea onInput={messageHandler} value={message} className="message-input-text form-control" name="" ></textarea>
            <button onClick={() => { clickHandler() }} className="message-input-button btn btn-primary">Send</button>
        </div>
    )
}
