import React from 'react';
import './index.css';

export default function OwnMessage(props) {
    const { id, createdAt, text } = props.messageElem

    const deleteHandler = () => {
        props.deleteF(id)
    }

    return (
        <li className="own-message">
            <span className="message-content">
                <span className="message-time">{createdAt}</span>
                <span className="message-text">{text}</span>
                <span className="own-btn-group">
                    <button className="message-edit btn btn-info">
                        Edit
                    </button>
                    <button className="message-delete btn btn-danger" onClick={() => { deleteHandler() }}>
                        Delete
                    </button>
                </span>
            </span>
        </li>
    )
}
