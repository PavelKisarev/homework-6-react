import React from 'react';
import './index.css';

export default function Preloader() {
    return (
        <div className="preloader">
            <div className="spinner-border text-primary" role="status">
                <span className="visually-hidden">Loading...</span>
            </div>
        </div>
    )
}
